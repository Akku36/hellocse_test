<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/stars', 'App\Http\Controllers\StarController@index');
Route::post('/stars', 'App\Http\Controllers\StarController@create');
Route::put('/stars/{id}', 'App\Http\Controllers\StarController@edit');
Route::delete('/stars/{id}', 'App\Http\Controllers\StarController@destroy');
