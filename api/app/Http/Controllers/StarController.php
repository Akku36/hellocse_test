<?php

namespace App\Http\Controllers;

use App\Models\Star;
use Illuminate\Http\Request;

class StarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // get all stars
        $stars = Star::all();
        $stars = Star::orderBy('lastname', 'asc')->get();
        return response()->json($stars, 200);
    }

    /**
     * Create a new resource.
     */
    public function create(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'image' => 'required',
            'description' => 'required'
        ]);
      

        // create a star

        $star = new Star();
        $star->firstname = ucfirst(strToLower($request->firstname));
        $star->lastname = ucfirst(strToLower($request->lastname));
        $star->image = $request->image;
        $star->description = ucfirst(strToLower($request->description));
        $star->save();

        return response()->json($star, 201);
   
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, Star $star)
    {
            // verify if the star exists 
        $star = Star::find($request->id);

        if (is_null($star)) {
            return response()->json([
                "message" => "star not found"
            ], 404);
        }
            // verify if fields are filled
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'image' => 'required',
            'description' => 'required'
        ]);
            // update the star
            $star->firstname = ucfirst(strToLower($request->firstname));
            $star->lastname = ucfirst(strToLower($request->lastname));
            $star->image = $request->image;
            $star->description = ucfirst(strToLower($request->description));
            $star->save();
            return response()->json($star, 200);

    }

 
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Star $star)
    {
        // verify if the star exists
        $star = Star::find($request->id);
        if (is_null($star)) {
            return response()->json([
                "message" => "star not found"
            ], 404);
        }
        // delete the star
        $star->delete();
        return response()->json([
            "message" => "star record deleted"
        ], 200);

    }
}
