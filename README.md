# How to setup this projet

Make sure to have PHP 8.1+ installed

## In your terminal start with API directory

Install API dependencies

```bash
  cd api && composer install
```

Create .env file based on .env.example and ajust database informations if needed

```bash
cp .env.example .env
```

Execute migrations

```bash
 php artisan migrate
```

Database seeding

```bash
  php artisan db:seed
```

Start API server

```bash
  php artisan serve
```

## In a second terminal move to client directory

From api directory

```bash
  cd ../client
```

Install dependencies

```bash
  npm install
```

Start client server

```bash
 npm run dev
```

Use the vite local URL to access the application in your browser

Use /admin URI to access hidden backoffice CRUD
