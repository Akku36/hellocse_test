/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js}'],
  darkMode: false,
  theme: {
    extend: {}
  },
  plugins: []
};
